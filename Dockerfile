ARG PYTHON_DOCKER_IMAGE
ARG POETRY_VERSION

# Use build layer
FROM python:${PYTHON_DOCKER_IMAGE} as builder

ARG PYTHON_DOCKER_IMAGE
ARG POETRY_VERSION

ENV POETRY_HOME=/opt/poetry

RUN echo PYTHON_DOCKER_IMAGE=${PYTHON_DOCKER_IMAGE} POETRY_VERSION=${POETRY_VERSION} 

RUN if [ -z "${PYTHON_DOCKER_IMAGE##*alpine*}" ]; then \
        echo "Installing required packages for alpine image"; \
        apk update; \
        apk add gcc musl-dev python3-dev libffi-dev openssl-dev cargo; \
    else \
        echo "Skipping installation of required packages for alpine image"; \
    fi

# Install poetry
ADD get-poetry.py /opt/get-poetry.py
RUN pip3 install --no-cache-dir --upgrade pip 
RUN python /opt/get-poetry.py --version ${POETRY_VERSION}

# Use runner layer
FROM python:${PYTHON_DOCKER_IMAGE}

# Disable creation of virtual environments to reduce image size
ENV POETRY_VIRTUALENVS_CREATE=false
ENV POETRY_HOME=/opt/poetry

# Copy poetry into image
COPY --from=builder /opt/poetry /opt/poetry
ENV PATH="${POETRY_HOME}/bin:${PATH}"

RUN pip3 install --no-cache-dir --upgrade pip 

CMD ["python3"]

