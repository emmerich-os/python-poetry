# python-poetry

Docker images for python with poetry.

The images are available on [DockerHub](https://hub.docker.com/r/fabianemmi/python-poetry).

## Releasing a new image

Releasing a set of new images (default, slim, buster, slim-buster, alpine) just requries to publish a new git tag in the form of `<Python version>-<Poetry version>`, e.g. `3.12-1.8.2`.

The push of the image with the `latest` tag is controlled by the CI variables `LATEST_PYTHON_VERSION` and `LATEST_POETRY_VERSION`.
Hence, if a new version is released, these have to be adapted to make sure to push the latest image.
